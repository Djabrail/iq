const data = [
  {
    id: 1,
    question: "ваш пол:",
    type: 1,
    answers: [
      {
        id: 1,
        text: "мужчина",
      },
      {
        id: 2,
        text: "женщина",
      },
    ],
  },
  {
    id: 2,
    question: "укажите ваш возраст:",
    type: 1,
    answers: [
      {
        id: 1,
        text: "До 18",
      },
      {
        id: 2,
        text: "От 18 до 28",
      },
      {
        id: 3,
        text: "от 29 до 35",
      },
      {
        id: 4,
        text: "От 36",
      },
    ],
  },
  {
    id: 3,
    question: "Выберите лишнее:",
    type: 1,
    answers: [
      {
        id: 1,
        text: "Дом",
      },
      {
        id: 2,
        text: "Шалаш",
      },
      {
        id: 3,
        text: "Бунгало",
      },
      {
        id: 4,
        text: "Скамейка",
      },
      {
        id: 5,
        text: "Хижина",
      },
    ],
  },
  {
    id: 4,
    question: "Выберите цвет, который сейчас наиболее Вам приятен:",
    type: 2,
    answers: [
      {
        id: 1,
        text: "#A8A8A8",
      },
      {
        id: 2,
        text: "#0000A9",
      },
      {
        id: 3,
        text: "#00A701",
      },
      {
        id: 4,
        text: "#F60100",
      },
      {
        id: 5,
        text: "#FDFF19",
      },
      {
        id: 3,
        text: "#A95403",
      },
      {
        id: 4,
        text: "#000000",
      },
      {
        id: 5,
        text: "#850068",
      },
      {
        id: 5,
        text: "#46B2AC",
      },
    ],
  },
];

export default data;
